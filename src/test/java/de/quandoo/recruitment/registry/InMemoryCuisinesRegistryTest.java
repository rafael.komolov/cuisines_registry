package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import de.quandoo.recruitment.registry.model.SupportedCuisine;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static de.quandoo.recruitment.registry.model.SupportedCuisine.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class InMemoryCuisinesRegistryTest {

    private InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

    @Before
    public void init() {
        addData("1", FRENCH);
        addData("2", GERMAN);
        addData("3", ITALIAN);
        addData("4", GERMAN);
        addData("5", GERMAN);
        addData("6", ITALIAN);
    }

    @Test
    public void testCuisineCustomers1() {
        List<Customer> result = cuisinesRegistry.cuisineCustomers(new Cuisine(FRENCH.getName()));
        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals("1", result.get(0).getUuid());
    }

    @Test
    public void testCuisineCustomers2() {
        List<Customer> result = cuisinesRegistry.cuisineCustomers(new Cuisine(GERMAN.getName()));
        assertNotNull(result);
        assertEquals(3, result.size());
    }

    @Test
    public void testCuisineCustomersNonExisting() {
        List<Customer> result = cuisinesRegistry.cuisineCustomers(new Cuisine("russian"));
        assertEquals(Collections.emptyList(), result);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void registerUnsupportedCuisine() {
        cuisinesRegistry.register(new Customer("1"), new Cuisine("russian"));
    }

    @Test
    public void testCuisineCustomersNull() {
        assertEquals(Collections.emptyList(), cuisinesRegistry.cuisineCustomers(null));
    }

    @Test
    public void testCustomerCuisinesNull() {
        assertEquals(Collections.emptyList(), cuisinesRegistry.customerCuisines(null));
    }

    @Test
    public void testTopCuisinesZeroNum() {
        assertEquals(Collections.emptyList(), cuisinesRegistry.topCuisines(0));
    }

    @Test
    public void testTopCuisinesNegativeNum() {
        assertEquals(Collections.emptyList(), cuisinesRegistry.topCuisines(-1));
    }

    @Test
    public void testTopCuisines1() {
        List<Cuisine> result = cuisinesRegistry.topCuisines(1);
        assertEquals(1, result.size());
        assertEquals(GERMAN.getName(), result.get(0).getName());
    }

    @Test
    public void testTopCuisines2() {
        List<Cuisine> result = cuisinesRegistry.topCuisines(2);
        assertEquals(2, result.size());
        assertEquals(GERMAN.getName(), result.get(0).getName());
        assertEquals(ITALIAN.getName(), result.get(1).getName());
    }

    @Test
    public void testTopCuisinesLargeNum() {
        List<Cuisine> result = cuisinesRegistry.topCuisines(10);
        assertEquals(3, result.size());
        assertEquals(GERMAN.getName(), result.get(0).getName());
        assertEquals(ITALIAN.getName(), result.get(1).getName());
        assertEquals(FRENCH.getName(), result.get(2).getName());
    }

    private void addData(String id, SupportedCuisine cuisine) {
        cuisinesRegistry.register(new Customer(id), new Cuisine(cuisine.getName()));
    }
}