package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import de.quandoo.recruitment.registry.model.SupportedCuisine;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {

    private final static Logger log = Logger.getLogger(InMemoryCuisinesRegistry.class.getName());

    private Map<Cuisine, List<Customer>> mapByCuisine = new ConcurrentHashMap<>();
    private Map<Customer, List<Cuisine>> mapByCustomer = new ConcurrentHashMap<>();

    @Override
    public void register(final Customer customer, final Cuisine cuisine) {
        checkCuisine(cuisine);
        mapByCuisine.putIfAbsent(cuisine, Collections.synchronizedList(new ArrayList<>()));
        mapByCuisine.get(cuisine).add(customer);
        mapByCustomer.putIfAbsent(customer, Collections.synchronizedList(new ArrayList<>()));
        mapByCustomer.get(customer).add(cuisine);
    }

    @Override
    public List<Customer> cuisineCustomers(final Cuisine cuisine) {
        if (cuisine == null) {
            return Collections.emptyList();
        }
        return mapByCuisine.getOrDefault(cuisine, Collections.emptyList());
    }

    @Override
    public List<Cuisine> customerCuisines(final Customer customer) {
        if (customer == null) {
            return Collections.emptyList();
        }
        return mapByCustomer.getOrDefault(customer, Collections.emptyList());
    }

    @Override
    public List<Cuisine> topCuisines(final int n) {
        if (n <= 0) {
            return Collections.emptyList();
        }
        List<Cuisine> topCuisines = mapByCuisine.entrySet().stream()
                .sorted(Collections.reverseOrder(Comparator.comparing(o -> o.getValue().size())))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
        int resultSize = Math.min(n, topCuisines.size());
        return topCuisines.subList(0, resultSize);
    }


    private void checkCuisine(Cuisine cuisine) {
        if (!SupportedCuisine.contains(cuisine.getName())) {
            String errMsg = String.format("Unknown cuisine '%s', please reach johny@bookthattable.de to update the code",
                    cuisine.getName());
            log.log(Level.SEVERE, errMsg);
            throw new UnsupportedOperationException(errMsg);
        }
    }
}
