package de.quandoo.recruitment.registry.model;

import java.util.HashSet;
import java.util.Set;

public enum SupportedCuisine {

    ITALIAN("italian"),
    FRENCH("french"),
    GERMAN("german");

    private final String name;

    SupportedCuisine(String name) {
        this.name = name;
    }

    private static Set<String> supportedCuisines = new HashSet<>();

    static {
        for (SupportedCuisine c : SupportedCuisine.values()) {
            supportedCuisines.add(c.name);
        }
    }
    public static boolean contains(String cuisine) {
        return supportedCuisines.contains(cuisine);
    }

    public String getName() {
        return name;
    }
}
